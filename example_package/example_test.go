package example

import (
	"os"
	"testing"
)

func ExamplePrintHello() {
	PrintHello()
	// Output:
	// hello world!
}

func TestGetVersionFromEnv(t *testing.T) {
	expected := "1.0"
	os.Setenv("VERSION", expected)
	ver := GetVersionFromEnv()

	if ver != expected {
		t.Fatalf("is %s expected %s", ver, expected)
	}

	os.Unsetenv("VERSION")

	if GetVersionFromEnv() != "unknown" {
		t.Fatalf("is %s expected unknown", ver)
	}
}
