package example

import (
	"fmt"
	"os"
)

// PrintHello prints hello msg
func PrintHello() {
	fmt.Println("hello world!")
}

// GetVersionFromEnv is used by deploy to get current version
func GetVersionFromEnv() string {
	ver := os.Getenv("VERSION")
	if ver == "" {
		return "unknown"
	}
	return ver
}
