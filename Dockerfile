# start from scratch
FROM alpine:3.7

# https://stackoverflow.com/questions/34729748/installed-go-binary-not-found-in-path-on-alpine-linux-docker
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
# copy our static linked executable
COPY releases/*  /go/src/

EXPOSE 8080

ENTRYPOINT [ "/go/src/golang-demo" ]


  