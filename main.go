package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	example "gitlab.com/FlakM/golang-demo/example_package"
)

var (
	dbURL = flag.String("db_url", os.Getenv("ROACH_URL"), "database url ie: postgresql://root@127.0.0.1:26257/books?sslmode=disable")
)

func main() {
	flag.Parse()

	example.PrintHello()

	db, err := gorm.Open("postgres", *dbURL)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Connected to db: [%s]\n", *dbURL)

	defer db.Close()

	db.Exec("create database if not exists books")

	// Automatically create the "books" table based on the Books model.
	db.AutoMigrate(&Books{})

	HomeHandler := func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello to my home page at version [%s] cześć Marcin", example.GetVersionFromEnv())
	}

	ReadAllBooks := func(w http.ResponseWriter, r *http.Request) {
		// Print out the balances.
		var books []Books
		db.Find(&books)
		json.NewEncoder(w).Encode(books)
	}

	CreateBook := func(w http.ResponseWriter, r *http.Request) {
		var u Books

		if r.Body == nil {
			http.Error(w, "Please send a request body", 400)
			return
		}

		err := json.NewDecoder(r.Body).Decode(&u)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		db.Create(&u)
	}

	DeleteBook := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		title := vars["title"]
		db.Where("title=?", title).Delete(Books{})
	}

	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/books/", ReadAllBooks).Methods("GET")
	r.HandleFunc("/books/", CreateBook).Methods("POST")
	r.HandleFunc("/books/{title}", DeleteBook).Methods("DELETE")
	http.ListenAndServe(":8080", r)
}

// Books are used to parse from/to json or db
type Books struct {
	Title  string `gorm:"primary_key"`
	Pages  int
	Author string
}
