[![pipeline status](https://gitlab.com/FlakM/golang-demo/badges/master/pipeline.svg)](https://gitlab.com/FlakM/golang-demo/commits/master)

[![coverage report](https://gitlab.com/FlakM/golang-demo/badges/master/coverage.svg)](https://gitlab.com/FlakM/golang-demo/commits/master)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/pantomath-io/demo-tools)](https://goreportcard.com/report/gitlab.com/FlakM/golang-demo)


# Using deployed application: 

To get external adress of service issue:

```bash
EXTERNAT_IP=$(kubectl get service books-deploy --namespace=app-stage  -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
```

navigate to url http://app.$EXTERNAL_IP.nip.io:8080/books/ to see list of books

or use prepared curl:

```bash
# get list of books
curl http://app.EXTERNAT_IP.nip.io:8080/books/ 

# get hello msg
curl -X POST http://app.EXTERNAT_IP.nip.io:8080/

# add books
curl -X POST http://app.EXTERNAT_IP.nip.io:8080/books/ -d '{"title":"ci cd is so hard", "pages":1000,"author":"Maciej Flak and blogs"}'

# delete book
curl -X DELETE http://app.EXTERNAT_IP.nip.io:8080/books/{title}
```




# Creating/deleting GKE cluster

```bash
gcloud container clusters create [CLUSTER_NAME]
gcloud container clusters list
gcloud container clusters get-credentials [CLUSTER_NAME]
gcloud container clusters delete [CLUSTER_NAME]
```


# Authenticating in GKE

execute below script

```bash
# Set these variables for your project
PROJECT_ID=my-project
SA_NAME=my-new-serviceaccount
SA_EMAIL=$SA_NAME@$PROJECT_ID.iam.gserviceaccount.com
KEY_FILE=~/serviceaccount_key.json
CLUSTER_NAME=my-cluster

# Create a new GCP IAM service account.
gcloud iam service-accounts create $SA_NAME

# Download a json key for that service account.
gcloud iam service-accounts keys create $KEY_FILE --iam-account $SA_EMAIL

# Give that service account the "Container Engine Developer" IAM role for your project.
gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:$SA_EMAIL --role roles/container.developer

# Configure kubectl to point to your cluster.
gcloud container clusters get-credentials $CLUSTER_NAME

# Configure Application Default Credentials (what kubectl uses) to use the service account.
export GOOGLE_APPLICATION_CREDENTIALS=$KEY_FILE
```

and add secrets to CICD

`cat ~/.kube/config | base64 | xclip -sel clip` to `kube_config`
`cat ${KEY_FILE} | base64 | xclip -sel clip` to `gcloud_config`

This is instruction taken directly from:

https://github.com/kubernetes/kubernetes/issues/30617#issuecomment-250594453